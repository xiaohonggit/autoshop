package com.shop.autoshop.mapper;


import com.shop.autoshop.pojo.WebConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface WebConfigMapper {
    WebConfig findWebConfig();
    void updateWeb(@Param("web") WebConfig webConfig);
    void updatePay(@Param("web") WebConfig webConfig);
}
