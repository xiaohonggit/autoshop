package com.shop.autoshop.mapper;

import com.shop.autoshop.pojo.Order;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


import java.util.Date;
import java.util.List;

@Repository
@Mapper
public interface OrderMapper {
   void addOrder(Integer pdId, String addtime,String payId,String orderId,Integer buyNum,Double money,String contact,String password,Integer payMethod,String goods,String gd);
   Order findByPayId(String payId);
   List<Order> findInvalidOrder();
   void delInvalidOrder(List<Integer> ids);
   void updateOrderStateAndTsNum(Integer id,Integer state,String tsNum);
   List<Order> findOrderByQuery(String contact,String password);
   List<Order> findPageList(int page,int pageSize);
   Integer odCount();
   Order findOrderById(Integer id);
   void delOrderById(Integer id);
   void delOrderByIds(int[] array);
   Order findOrderByPayIdOrTsNum(String num);


}
