package com.shop.autoshop.mapper;

import com.shop.autoshop.pojo.EditAdmin;
import com.shop.autoshop.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserMapper {
    User findUserByName(String name);
    User findUserById(Integer id);
    void updateUser(@Param("editAdmin")EditAdmin editAdmin);
}
