package com.shop.autoshop.mapper;

import com.shop.autoshop.pojo.PdClass;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ClassMapper {
    List<PdClass> findAllClass();
    List<PdClass> findNormalClass();
    PdClass findClassById(int id);
    void addClass(String name,String des,String addtime,int state,int user);
    void editClass(int id,String name,String des,int state);
    void delClass(int id);
}
