package com.shop.autoshop.mapper;

import com.shop.autoshop.pojo.Product;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import java.util.List;
@Repository
@Mapper
public interface ProductMapper {
    void add(String name, Integer type,String gd,Integer gdNum, Double price, Double price2, String pic, String des, String addtime, Integer pdClass,Integer state, Integer user);
    List<Product> findAll();
    Product findById(int id);
    void updataById(int id,String name, Integer type,String gd,Integer gdNum, Double price, Double price2, String pic, String des,Integer pdClass,Integer state);
    void delById(int id);
    void delByIds(int[] array);
    List<Product> findPageList(int page,int pageSize);
    Integer pdCount();
    List<Product> findKmList();
    List<Product> findIndexAll();
    Product findPdById(Integer id);
    Product findPdLockById(Integer id);
    void addPdGdNum(Integer id,Integer num);
    void redPdGdNum(Integer id,Integer num);
}
