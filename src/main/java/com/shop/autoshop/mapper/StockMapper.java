package com.shop.autoshop.mapper;

import com.shop.autoshop.pojo.Stock;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface StockMapper {
    List<Stock> findAll(Integer page,Integer pageSize,Integer pdId,Integer state);
    Integer listCount(Integer pdId,Integer state);
    int insertList(@Param("stocks") List<Stock> stocks);
    void delById(int id);
    void delByIds(int[] array);
    List<Stock> findByState(int pdId,int state,int needNum);
    void updateStockState(int[] id,int state);
    void updateStockStates(List<Integer> id,int state);
    Stock findById(Integer id);
}
