package com.shop.autoshop.interceptor;

import com.shop.autoshop.pojo.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class AdminInterceptor implements HandlerInterceptor {
    private Log log = LogFactory.getLog(getClass());
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession httpSession = request.getSession();
        User user = (User)httpSession.getAttribute("USER");
        if(user == null){
            log.warn("登录权限不足，IP："+request.getRemoteAddr());
            response.sendRedirect("/");
            return false;
        }else{
            if(user.getUserPower() > 3){
                log.info("登录权限不够，IP："+request.getRemoteAddr());
                response.sendRedirect("/");
                return false;
            }else {
                log.info("权限通过，IP："+request.getRemoteAddr());
                return true;
            }
        }

    }

}
