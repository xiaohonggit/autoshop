package com.shop.autoshop;

import com.shop.autoshop.service.OrderService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class AutoshopApplication {
    private Log log = LogFactory.getLog(getClass());

    @Autowired
    private OrderService orderService;

    public static void main(String[] args) {
        SpringApplication.run(AutoshopApplication.class, args);
    }

    @Scheduled(fixedDelay = 60000)
    public void  orderInspect(){
        log.info("定时验证订单中......");
        orderService.orderAllInspect();
    }
}
