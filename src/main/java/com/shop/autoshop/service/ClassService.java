package com.shop.autoshop.service;

import com.shop.autoshop.pojo.PdClass;

import java.util.List;

public interface ClassService {
    List<PdClass> getAll();
    List<PdClass> getNormalAll();
    PdClass getById(int id);
    void add(String name,String des,String addtime,int state,int user);
    void editClass(int id,String name,String des,int state);
    void delClass(int id);
}
