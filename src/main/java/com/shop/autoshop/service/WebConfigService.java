package com.shop.autoshop.service;

import com.shop.autoshop.pojo.WebConfig;

public interface WebConfigService {
    WebConfig getWebConfig();
    void updateWeb(WebConfig webConfig);
    void updatePay(WebConfig webConfig);
}
