package com.shop.autoshop.service;

import com.shop.autoshop.pojo.Product;

import java.util.List;
import java.util.Map;

public interface ProductService {
    void addProduct(Product product);
    List<Product> getAll();
    Product getById(int id);
    void updataById(Product product);
    void delByIds(Integer id,String ids);
    Map<String,Object> getPageList(int currentPage, int pageSize);
    List<Product> findKmList();
    List<Product> findIndexAll();
    Product findPdById(Integer id);
    Product findPdLockById(Integer id);
    void addPdGdNum(Integer id,Integer num);
    void redPdGdNum(Integer id,Integer num);
}
