package com.shop.autoshop.service;

import com.shop.autoshop.pojo.Stock;

import java.util.List;
import java.util.Map;

public interface StockService {
    Map<String,Object> getPageList(Integer pdId ,Integer state,Integer currentPage,Integer pageSize);
    int insertList(Integer pdId,String text);
    void delByIds(Integer id,String ids);
    String addLock(int pdId,int state,int needNum);
    void updateStockState(int[] id,int state);
}
