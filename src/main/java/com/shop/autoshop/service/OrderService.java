package com.shop.autoshop.service;

import com.shop.autoshop.pojo.Order;
import com.shop.autoshop.pojo.Query;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface OrderService {
    void addOrder(Integer pdId, String addtime, String payId, String orderId, Integer buyNum,Double money, String contact, String password, Integer payMethod,String goods,String gd);
    Order findByPayId(String payId);
    void orderAllInspect();
    void updateOrderStateAndTsNum(Integer id,Integer state,String tsNum);
    List<Query> findAllOrderByQuery(String contact,String password);
    Map<String,Object> getPageList(int currentPage, int pageSize);
    Order findOrderById(Integer id);
    void delOrderByIds(Integer id,String ids);
    Order findOrderByPayIdOrTsNum(String num);
}
