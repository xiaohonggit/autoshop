package com.shop.autoshop.service.imp;

import com.shop.autoshop.mapper.ProductMapper;
import com.shop.autoshop.mapper.StockMapper;
import com.shop.autoshop.pojo.Product;
import com.shop.autoshop.pojo.Stock;
import com.shop.autoshop.service.StockService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service("stockService")
public class StockServiceImp implements StockService {
    @Autowired
    private StockMapper stockMapper;
    @Autowired
    private ProductMapper productMapper;

    @Override
    public Map<String, Object> getPageList(Integer pdId, Integer state, Integer currentPage, Integer pageSize) {
        //文章列表
        Map<String,Object> map = new HashMap<>();
        int page = (currentPage-1)*pageSize;
        List<Stock> stList = stockMapper.findAll(page,pageSize,pdId,state);
        map.put("list",stList);

        //分页信息
        StringBuffer html = new StringBuffer();
        int listCount = stockMapper.listCount(pdId,state);
        int pageCount = listCount/pageSize;
        if(listCount%pageSize != 0){
            pageCount += 1;
        }
        html.append("<li class=\"disabled\"><span>"+currentPage+" / "+pageCount+"</span></li>");
        if(currentPage > 1){
            html.append("<li><a href=\"?page=1\">«</a></li>");
            html.append("<li><a href=\"?page="+(currentPage-1)+"\">‹</a></li>");
        }
        if(pageCount <= 10){
            for(int i = 1;i <= pageCount;i++){
                if(i == currentPage){
                    html.append("<li class=\"active\"><span>"+i+"</span></li>");
                }else{
                    html.append("<li><a href=\"?page="+i+"\">"+i+"</a></li>");
                }
            }
        }else{
            if(currentPage >= 10){
                if(currentPage+5 <= pageCount){
                    for(int i = currentPage-4;i <= currentPage+5;i++){
                        if(i == currentPage){
                            html.append("<li class=\"active\"><span>"+i+"</span></li>");
                        }else{
                            html.append("<li><a href=\"?page="+i+"\">"+i+"</a></li>");
                        }
                    }
                }else{
                    for(int i = currentPage-4;i <= pageCount;i++){
                        if(i == currentPage){
                            html.append("<li class=\"active\"><span>"+i+"</span></li>");
                        }else{
                            html.append("<li><a href=\"?page="+i+"\">"+i+"</a></li>");
                        }
                    }
                }
            }else{
                for(int i = 1;i <= 10;i++){
                    if(i == currentPage){
                        html.append("<li class=\"active\"><span>"+i+"</span></li>");
                    }else{
                        html.append("<li><a href=\"?page="+i+"\">"+i+"</a></li>");
                    }
                }
            }
        }
        if(currentPage < pageCount){
            html.append("<li><a href=\"?page="+(currentPage+1)+"\">›</a></li>");
            html.append("<li><a href=\"?page="+pageCount+"\" >»</a></li>");
        }

        map.put("page",html.toString());


        return map;

    }

    @Override
    public int insertList(Integer pdId,String text) {
        String[] splitText = text.split("\n");
        List<Stock> list = new ArrayList<>();
        for (String t : splitText) {
            if (t.trim() == null || t.trim() == ""){
                continue;
            }
            String[] splitT = t.split("----");
            Stock stock = new Stock();
            stock.setStPdId(pdId);
            stock.setStKh(splitT[0]);
            stock.setStMm(splitT[1]);
            stock.setStAddtime(new Date());
            list.add(stock);

        }
        int res = stockMapper.insertList(list);
        return res;
    }

    @Override
    public void delByIds(Integer id, String ids) {
        if(id != null){
            stockMapper.delById(id);
        }else if (ids != null && ids != ""){
            String[] array = ids.split(",");
            int[] intArr = new int[array.length];
            for (int i = 0; i < array.length; i++) {
                intArr[i] = Integer.parseInt(array[i]);
            }
            stockMapper.delByIds(intArr);
        }
    }

    @Transactional
    @Override
    public String  addLock(int pdId, int state, int needNum) {
        Product pd = productMapper.findPdLockById(pdId);
        if(pd.getPdType() == 0){
            //卡密
            List<Stock> sk = stockMapper.findByState(pdId, state, needNum);
            if(sk.size() < needNum){
                //库存不足
                return "0";
            }
            int[] ids = new int[sk.size()];
            int i = 0;
            for (Stock s : sk) {
                ids[i] = s.getStId();
                i++;
            }
            stockMapper.updateStockState(ids,state);
            String goods = StringUtils.join(ids,',');
            //System.out.println(goods);
            return goods;
        }else{
            //固定
            if(pd.getPdGdNum() <  needNum){
                //库存不足
                return "0";
            }
            productMapper.redPdGdNum(pdId,1);
            return pd.getPdGd();
        }

    }

    @Override
    public void updateStockState(int[] id, int state) {
        stockMapper.updateStockState(id,state);
    }
}
