package com.shop.autoshop.service.imp;

import com.shop.autoshop.mapper.ClassMapper;
import com.shop.autoshop.pojo.PdClass;
import com.shop.autoshop.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("classService")
public class ClassServiceImp implements ClassService {
    @Autowired
    private ClassMapper classMapper;

    @Override
    public List<PdClass> getAll() {
        return classMapper.findAllClass();
    }

    @Override
    public List<PdClass> getNormalAll() {
        return classMapper.findNormalClass();
    }

    @Override
    public PdClass getById(int id) {
        return classMapper.findClassById(id);
    }

    @Override
    public void add(String name, String des, String addtime, int state, int user) {
        classMapper.addClass(name,des,addtime,state,user);
    }

    @Override
    public void editClass(int id, String name, String des, int state) {
        classMapper.editClass(id,name,des,state);
    }

    @Override
    public void delClass(int id) {
        classMapper.delClass(id);
    }
}
