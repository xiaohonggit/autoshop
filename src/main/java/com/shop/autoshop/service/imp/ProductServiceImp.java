package com.shop.autoshop.service.imp;

import com.shop.autoshop.mapper.ProductMapper;
import com.shop.autoshop.pojo.Product;
import com.shop.autoshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("productService")
public class ProductServiceImp implements ProductService {
    @Autowired
    private ProductMapper productMapper;

    @Override
    public void addProduct(Product product) {
        //设置日期格式
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String addtime = df.format(new Date());
        productMapper.add(product.getPdName(),product.getPdType(),product.getPdGd(),product.getPdGdNum(),product.getPdPrice(),product.getPdPrice2(),product.getPdPic(),product.getPdDes(),addtime,product.getPdClass(),product.getPdState(),product.getPdUser());
    }

    @Override
    public List<Product> getAll() {
        return productMapper.findAll();
    }

    @Override
    public Product getById(int id) {
        return productMapper.findById(id);
    }

    @Override
    public void updataById(Product product) {
        productMapper.updataById(product.getPdId(),product.getPdName(),product.getPdType(),product.getPdGd(),product.getPdGdNum(),product.getPdPrice(),product.getPdPrice2(),product.getPdPic(),product.getPdDes(),product.getPdClass(),product.getPdState());
    }

    @Override
    public void delByIds(Integer id, String ids) {
        if(id != null){
            productMapper.delById(id);
        }else if (ids != null && ids != ""){
            String[] array = ids.split(",");
            int[] intArr = new int[array.length];
            for (int i = 0; i < array.length; i++) {
                intArr[i] = Integer.parseInt(array[i]);
            }
            productMapper.delByIds(intArr);
        }
    }

    @Override
    public Map<String, Object> getPageList(int currentPage, int pageSize) {
        //文章列表
        Map<String,Object> map = new HashMap<>();
        int page = (currentPage-1)*pageSize;
        List<Product> pdList = productMapper.findPageList( page, pageSize);
        map.put("list",pdList);

        //分页信息
        StringBuffer html = new StringBuffer();
        int listCount = productMapper.pdCount();
        int pageCount = listCount/pageSize;
        if(listCount%pageSize != 0){
            pageCount += 1;
        }
        html.append("<li class=\"disabled\"><span>"+currentPage+" / "+pageCount+"</span></li>");
        if(currentPage > 1){
            html.append("<li><a href=\"?page=1\">«</a></li>");
            html.append("<li><a href=\"?page="+(currentPage-1)+"\">‹</a></li>");
        }
        if(pageCount <= 10){
            for(int i = 1;i <= pageCount;i++){
                if(i == currentPage){
                    html.append("<li class=\"active\"><span>"+i+"</span></li>");
                }else{
                    html.append("<li><a href=\"?page="+i+"\">"+i+"</a></li>");
                }
            }
        }else{
            if(currentPage >= 10){
                if(currentPage+5 <= pageCount){
                    for(int i = currentPage-4;i <= currentPage+5;i++){
                        if(i == currentPage){
                            html.append("<li class=\"active\"><span>"+i+"</span></li>");
                        }else{
                            html.append("<li><a href=\"?page="+i+"\">"+i+"</a></li>");
                        }
                    }
                }else{
                    for(int i = currentPage-4;i <= pageCount;i++){
                        if(i == currentPage){
                            html.append("<li class=\"active\"><span>"+i+"</span></li>");
                        }else{
                            html.append("<li><a href=\"?page="+i+"\">"+i+"</a></li>");
                        }
                    }
                }
            }else{
                for(int i = 1;i <= 10;i++){
                    if(i == currentPage){
                        html.append("<li class=\"active\"><span>"+i+"</span></li>");
                    }else{
                        html.append("<li><a href=\"?page="+i+"\">"+i+"</a></li>");
                    }
                }
            }
        }
        if(currentPage < pageCount){
            html.append("<li><a href=\"?page="+(currentPage+1)+"\">›</a></li>");
            html.append("<li><a href=\"?page="+pageCount+"\" >»</a></li>");
        }

        map.put("page",html.toString());


        return map;
    }

    @Override
    public List<Product> findKmList() {
        return productMapper.findKmList();
    }

    @Override
    public List<Product> findIndexAll() {
        return productMapper.findIndexAll();
    }

    @Override
    public Product findPdById(Integer id) {
        return productMapper.findPdById(id);
    }

    @Override
    public Product findPdLockById(Integer id) {
        return productMapper.findPdLockById(id);
    }

    @Override
    public void addPdGdNum(Integer id, Integer num) {
        productMapper.addPdGdNum(id,num);
    }

    @Override
    public void redPdGdNum(Integer id, Integer num) {
        productMapper.redPdGdNum(id,num);
    }
}
