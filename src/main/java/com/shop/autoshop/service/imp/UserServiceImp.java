package com.shop.autoshop.service.imp;

import com.shop.autoshop.mapper.UserMapper;
import com.shop.autoshop.pojo.EditAdmin;
import com.shop.autoshop.pojo.User;
import com.shop.autoshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

@Service("userService")
public class UserServiceImp implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserByName(String name) {
        return userMapper.findUserByName(name);
    }

    @Override
    public String editUser(EditAdmin editAdmin) {
        User u = userMapper.findUserById(editAdmin.getUserId());
        if(u == null){
            return "-2";//未知错误
        }
        String encodeStr = DigestUtils.md5DigestAsHex(editAdmin.getOldPassword().getBytes());
        if(encodeStr.equalsIgnoreCase(u.getUserPassword())){
            //密码正确
            editAdmin.setNewPassword(DigestUtils.md5DigestAsHex(editAdmin.getNewPassword().getBytes()));
            userMapper.updateUser(editAdmin);
            return "1";//修改成功

        }else{
            return "-1";//旧密码错误
        }
    }

}
