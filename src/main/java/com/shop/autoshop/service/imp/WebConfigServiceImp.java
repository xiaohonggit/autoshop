package com.shop.autoshop.service.imp;

import com.shop.autoshop.mapper.WebConfigMapper;
import com.shop.autoshop.pojo.WebConfig;
import com.shop.autoshop.service.WebConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("webConfigService")
public class WebConfigServiceImp implements WebConfigService {
    @Autowired
    private WebConfigMapper webConfigMapper;


    @Override
    public WebConfig getWebConfig() {
        return webConfigMapper.findWebConfig();
    }

    @Override
    public void updateWeb(WebConfig webConfig) {
        webConfigMapper.updateWeb(webConfig);
    }

    @Override
    public void updatePay(WebConfig webConfig) {
        webConfigMapper.updatePay(webConfig);
    }
}
