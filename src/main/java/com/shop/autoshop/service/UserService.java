package com.shop.autoshop.service;

import com.shop.autoshop.pojo.EditAdmin;
import com.shop.autoshop.pojo.User;

public interface UserService {
    User getUserByName(String name);
    String editUser(EditAdmin editAdmin);
}
