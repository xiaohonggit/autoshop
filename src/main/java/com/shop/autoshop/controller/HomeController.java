package com.shop.autoshop.controller;


import com.shop.autoshop.pojo.Query;
import com.shop.autoshop.service.OrderService;
import com.shop.autoshop.service.ProductService;
import com.shop.autoshop.service.WebConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/")
public class HomeController {
    @Autowired
    private WebConfigService webConfigService;
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderService orderService;


    @RequestMapping("/")
    public String HomePage(Model model){

        model.addAttribute("webConfig",webConfigService.getWebConfig());
        model.addAttribute("pdlist",productService.findIndexAll());
        return "index";
    }

    @RequestMapping("/uploads/{imgName}")
    public void uploads(@PathVariable("imgName") String imgName, HttpServletResponse response){
        String path = "D:\\webimg\\";
        String type=imgName.indexOf(".")!=-1?imgName.substring(imgName.lastIndexOf(".")+1, imgName.length()):null;
        //System.out.println(type);
        type = type.toLowerCase();
        if(type == null){
            try {
                response.sendError(404);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if (!type.equals("jpg") && !type.equals("png") && !type.equals("gif") && !type.equals("jpeg")){
            try {
                response.sendError(404);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileInputStream fis;
        response.setContentType("image/"+type+";charset=UTF-8");
        try {
            fis = new FileInputStream(path+imgName);
            int i = fis.available();
            byte[] data = new byte[i];
            fis.read(data);
            fis.close();

            //写图片
            OutputStream outputStream=new BufferedOutputStream(response.getOutputStream());
            outputStream.write(data);
            outputStream.flush();
            outputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @GetMapping("/pd/{id}")
    public String product(@PathVariable("id") Integer id,Model model){
        model.addAttribute("pd",productService.findPdById(id));
        return "pd";
    }

    @GetMapping("/order")
    public String order(int id,int num,String contact,String password,int pay,Model model){
        if(num < 1){
            num = 1;
        }
        Map<String,Object> map = new HashMap<>();
        map.put("num",num);
        map.put("contact",contact);
        map.put("password",password);
        map.put("pay",pay);

        model.addAttribute("order",map);
        model.addAttribute("pd",productService.findPdById(id));
        return "order";
    }

    @GetMapping("/query")
    public String query(){
        return "query/query";
    }

    @GetMapping("/queryList")
    public String queryList(String contact, String password, HttpSession httpSession,Model model){
        if(httpSession.getAttribute("queryVfCode") == null){
            return "redirect:/query";
        }else if((int)httpSession.getAttribute("queryVfCode") != 1){
            return "redirect:/query";
        }else if(contact.trim() == "" || password.trim() == ""){
            return "redirect:/query";
        }

        List<Query> list = orderService.findAllOrderByQuery(contact, password);
        if(list == null){
            return "redirect:/query";
        }

        httpSession.setAttribute("queryVfCode",0);
        model.addAttribute("list",list);
        return "query/list";
    }


}
