package com.shop.autoshop.controller;


import com.shop.autoshop.pojo.Order;
import com.shop.autoshop.pojo.Product;
import com.shop.autoshop.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.jws.WebParam;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private ClassService classService;
    @Autowired
    private ProductService productService;
    @Autowired
    private StockService stockService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private WebConfigService webConfigService;

    @GetMapping(value = {"/",""})
    public String adminHome(){
        return "admin/index";
    }

    @GetMapping("/welcome")
    public String adminWelcome(){
        return "admin/welcome";
    }

    @GetMapping("/class")
    public String adminClass(Model model){

        model.addAttribute("classList",classService.getAll());
        return "admin/class";
    }

    @GetMapping("/classEdit")
    public String classEdit(Integer id,Model model){

        model.addAttribute("class",classService.getById(id));
        return "admin/class-edit";
    }

    @GetMapping("/classAdd")
    public String classAdd(){

        return "admin/class-add";
    }

    @GetMapping("/product")
    public String product(Integer page,Model model){
        if(page == null){
            page = 1;
        }

        model.addAttribute("pagelist",productService.getPageList(page, 10));
        return "admin/product";
    }

    @GetMapping("/productAdd")
    public String productAdd(Model model){
        model.addAttribute("classList",classService.getNormalAll());
        return "admin/product-add";
    }

    @GetMapping("/productEdit")
    public String productEdit(Integer id,Model model){
        model.addAttribute("classList",classService.getNormalAll());
        model.addAttribute("pd",productService.getById(id));
        return "admin/product-edit";
    }

    @GetMapping(value = {"/stock/{pdId}/{state}","/stock"})
    public String stock(@PathVariable(value = "pdId",required = false) Integer pdId , @PathVariable(value = "state",required = false) Integer state,Integer page,Model model){
        if(page == null){
            page = 1;
        }
        model.addAttribute("pd",productService.findKmList());
        model.addAttribute("pageList",stockService.getPageList(pdId,state,page,10));
        //System.out.println(stockService.getPageList(pdId,state,page,1).toString());
        return "admin/stock";
    }

    @GetMapping("/stockAdd")
    public String stockeAdd(Model model){
        model.addAttribute("pd",productService.findKmList());
        return "admin/stock-add";
    }

    @GetMapping("/order")
    public String order(Integer page,Model model){
        if(page == null){
            page = 1;
        }

        model.addAttribute("pageList",orderService.getPageList(page,10));
        return "admin/order";
    }

    @GetMapping("/orderCheck")
    public String orderCheck(Integer id,Model model){
        Order od  = orderService.findOrderById(id);
        model.addAttribute("od",od);
        return "/admin/order-check";
    }

    @GetMapping("/orderQuery")
    public String orderQuery(String num,Model model){
        Order od = orderService.findOrderByPayIdOrTsNum(num);

        model.addAttribute("od",od);
        return "admin/order-query";
    }

    @GetMapping("/information")
    public String information(){

        return "admin/information";
    }

    @GetMapping("/system")
    public String system(Model model){
        model.addAttribute("web",webConfigService.getWebConfig());
        return "admin/system";
    }


}
