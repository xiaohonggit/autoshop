package com.shop.autoshop.controller;

import com.shop.autoshop.pojo.*;
import com.shop.autoshop.service.*;
import com.shop.autoshop.service.imp.ProductServiceImp;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/admin/ajax")
public class AjaxAdminController {
    @Autowired
    private ClassService classService;
    @Autowired
    private ProductService productService;
    @Autowired
    private UploadService uploadService;
    @Autowired
    private StockService stockService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;
    @Autowired
    private WebConfigService webConfigService;

    @ResponseBody
    @PostMapping("/addClass")
    public String addClass(PdClass pdClass,HttpSession httpSession){
        User user = (User) httpSession.getAttribute("USER");
        int userId = user.getUserId();
        //设置日期格式
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String addtime = df.format(new Date());
        classService.add(pdClass.getClName(),pdClass.getClDes(),addtime,pdClass.getClState(),userId);
        return "1";
    }

    @ResponseBody
    @PostMapping("/editClass")
    public String editClass(Integer id,String className, String classDes,Integer classStatus, HttpSession httpSession){
        classService.editClass(id,className,classDes,classStatus);
        return "1";
    }

    @ResponseBody
    @PostMapping("/delClass")
    public String delClass(Integer id){
        classService.delClass(id);
        return "1";
    }

    @ResponseBody
    @PostMapping("/upload")
    public String upload(MultipartFile file){
        String res = uploadService.uploadImg(file);
        return res;
    }

    @ResponseBody
    @PostMapping("/addProduct")
    public String addProduct(Product product,HttpServletRequest request){
        User user = (User)request.getSession().getAttribute("USER");
        product.setPdUser(user.getUserId());
        productService.addProduct(product);
        return "1";
    }

    @ResponseBody
    @PostMapping("/editProduct")
    public String editProduct(Product product){
        productService.updataById(product);
        return "1";
    }


    @ResponseBody
    @PostMapping("/delProduct")
    public String delProduct(Integer id,String ids){
        productService.delByIds(id,ids);
        return "1";
    }

    @ResponseBody
    @PostMapping("/addStock")
    public String addStock(Integer pdId,String km){

        int res = stockService.insertList(pdId, km);
        return ""+res;
    }

    @ResponseBody
    @PostMapping("/editStock")
    public String editStock(){

        return "";
    }

    @ResponseBody
    @PostMapping("/delStock")
    public String delStock(Integer id,String ids){
        stockService.delByIds(id,ids);
        return "1";
    }

    @ResponseBody
    @PostMapping("/delOrder")
    public String delOrder(Integer id,String ids){
        orderService.delOrderByIds(id,ids);
        return "1";
    }

    @ResponseBody
    @PostMapping("/editAdmin")
    public String editAdmin(EditAdmin editAdmin,HttpSession httpSession){
        User u = (User)httpSession.getAttribute("USER");
        if(editAdmin.getUserId() != u.getUserId()){
            editAdmin.setUserId(u.getUserId());
        }
        String res = userService.editUser(editAdmin);
        if(res.equals("1")){
            httpSession.removeAttribute("USER");
        }
        return res;
    }


    @ResponseBody
    @PostMapping("/editWeb")
    public String editWeb(WebConfig webConfig, Model model){
        webConfigService.updateWeb(webConfig);
        return "1";
    }

    @ResponseBody
    @PostMapping("/editPay")
    public String editPay(WebConfig webConfig, Model model){
        webConfigService.updatePay(webConfig);
        return "1";
    }


}
