package com.shop.autoshop.pojo;

import java.util.Date;

public class Stock {
    private Integer stId;
    private Integer stPdId;
    private String stKh;
    private String stMm;
    private Date stAddtime;
    private Integer stState;
    private String pdName;

    public Integer getStId() {
        return stId;
    }

    public void setStId(Integer stId) {
        this.stId = stId;
    }

    public Integer getStPdId() {
        return stPdId;
    }

    public void setStPdId(Integer stPdId) {
        this.stPdId = stPdId;
    }

    public String getStKh() {
        return stKh;
    }

    public void setStKh(String stKh) {
        this.stKh = stKh;
    }

    public String getStMm() {
        return stMm;
    }

    public void setStMm(String stMm) {
        this.stMm = stMm;
    }

    public Date getStAddtime() {
        return stAddtime;
    }

    public void setStAddtime(Date stAddtime) {
        this.stAddtime = stAddtime;
    }

    public Integer getStState() {
        return stState;
    }

    public void setStState(Integer stState) {
        this.stState = stState;
    }

    public String getPdName() {
        return pdName;
    }

    public void setPdName(String pdName) {
        this.pdName = pdName;
    }
}
