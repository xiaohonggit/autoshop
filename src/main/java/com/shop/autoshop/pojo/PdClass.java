package com.shop.autoshop.pojo;

import java.util.Date;

public class PdClass {
    Integer clId;
    String clName;
    String clDes;
    Date clAddtime;
    Integer clState;
    Integer clUser;

    public Integer getClId() {
        return clId;
    }

    public void setClId(Integer clId) {
        this.clId = clId;
    }

    public String getClName() {
        return clName;
    }

    public void setClName(String clName) {
        this.clName = clName;
    }

    public String getClDes() {
        return clDes;
    }

    public void setClDes(String clDes) {
        this.clDes = clDes;
    }

    public Date getClAddtime() {
        return clAddtime;
    }

    public void setClAddtime(Date clAddtime) {
        this.clAddtime = clAddtime;
    }

    public Integer getClState() {
        return clState;
    }

    public void setClState(Integer clState) {
        this.clState = clState;
    }

    public Integer getClUser() {
        return clUser;
    }

    public void setClUser(Integer clUser) {
        this.clUser = clUser;
    }
}
