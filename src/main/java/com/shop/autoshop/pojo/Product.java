package com.shop.autoshop.pojo;

import java.util.Date;

public class Product {
    Integer pdId;
    String pdName;
    Integer pdType;
    String pdGd;
    Integer pdGdNum;
    Double pdPrice;
    Double pdPrice2;
    String pdPic;
    String pdDes;
    Date pdAddtime;
    Integer pdClass;
    Integer pdState;
    Integer pdUser;
    String clName;
    Integer stNum;

    public Integer getPdId() {
        return pdId;
    }

    public void setPdId(Integer pdId) {
        this.pdId = pdId;
    }

    public String getPdName() {
        return pdName;
    }

    public void setPdName(String pdName) {
        this.pdName = pdName;
    }

    public Integer getPdType() {
        return pdType;
    }

    public void setPdType(Integer pdType) {
        this.pdType = pdType;
    }

    public Double getPdPrice() {
        return pdPrice;
    }

    public void setPdPrice(Double pdPrice) {
        this.pdPrice = pdPrice;
    }

    public String getPdPic() {
        return pdPic;
    }

    public void setPdPic(String pdPic) {
        this.pdPic = pdPic;
    }

    public String getPdDes() {
        return pdDes;
    }

    public void setPdDes(String pdDes) {
        this.pdDes = pdDes;
    }

    public Date getPdAddtime() {
        return pdAddtime;
    }

    public void setPdAddtime(Date pdAddtime) {
        this.pdAddtime = pdAddtime;
    }

    public Integer getPdClass() {
        return pdClass;
    }

    public void setPdClass(Integer pdClass) {
        this.pdClass = pdClass;
    }

    public Integer getPdState() {
        return pdState;
    }

    public void setPdState(Integer pdState) {
        this.pdState = pdState;
    }

    public Integer getPdUser() {
        return pdUser;
    }

    public void setPdUser(Integer pdUser) {
        this.pdUser = pdUser;
    }

    public Double getPdPrice2() {
        return pdPrice2;
    }

    public void setPdPrice2(Double pdPrice2) {
        this.pdPrice2 = pdPrice2;
    }

    public String getPdGd() {
        return pdGd;
    }

    public void setPdGd(String pdGd) {
        this.pdGd = pdGd;
    }

    public String getClName() {
        return clName;
    }

    public void setClName(String clName) {
        this.clName = clName;
    }

    public Integer getStNum() {
        return stNum;
    }

    public void setStNum(Integer stNum) {
        this.stNum = stNum;
    }

    public Integer getPdGdNum() {
        return pdGdNum;
    }

    public void setPdGdNum(Integer pdGdNum) {
        this.pdGdNum = pdGdNum;
    }
}
