package com.shop.autoshop.pojo;

import java.util.Date;

public class Order {
    private Integer odId;
    private Integer odPdId;
    private Date odAddtime;
    private String odPayId;
    private String odOrderId;
    private String odTsNum;
    private Integer odState;
    private Integer odBuyNum;
    private Double odMoney;
    private String odGoodId;
    private String odContact;
    private String odPassword;
    private Integer odPayMethod;
    private String odGd;
    private String pdName;

    public Integer getOdId() {
        return odId;
    }

    public void setOdId(Integer odId) {
        this.odId = odId;
    }

    public Integer getOdPdId() {
        return odPdId;
    }

    public void setOdPdId(Integer odPdId) {
        this.odPdId = odPdId;
    }

    public Date getOdAddtime() {
        return odAddtime;
    }

    public void setOdAddtime(Date odAddtime) {
        this.odAddtime = odAddtime;
    }

    public String getOdPayId() {
        return odPayId;
    }

    public void setOdPayId(String odPayId) {
        this.odPayId = odPayId;
    }

    public String getOdOrderId() {
        return odOrderId;
    }

    public void setOdOrderId(String odOrderId) {
        this.odOrderId = odOrderId;
    }

    public Integer getOdState() {
        return odState;
    }

    public void setOdState(Integer odState) {
        this.odState = odState;
    }

    public Integer getOdBuyNum() {
        return odBuyNum;
    }

    public void setOdBuyNum(Integer odBuyNum) {
        this.odBuyNum = odBuyNum;
    }

    public String getOdGoodId() {
        return odGoodId;
    }

    public void setOdGoodId(String odGoodId) {
        this.odGoodId = odGoodId;
    }

    public String getOdContact() {
        return odContact;
    }

    public void setOdContact(String odContact) {
        this.odContact = odContact;
    }

    public String getOdPassword() {
        return odPassword;
    }

    public void setOdPassword(String odPassword) {
        this.odPassword = odPassword;
    }

    public Integer getOdPayMethod() {
        return odPayMethod;
    }

    public void setOdPayMethod(Integer odPayMethod) {
        this.odPayMethod = odPayMethod;
    }

    public String getOdTsNum() {
        return odTsNum;
    }

    public void setOdTsNum(String odTsNum) {
        this.odTsNum = odTsNum;
    }

    public Double getOdMoney() {
        return odMoney;
    }

    public void setOdMoney(Double odMoney) {
        this.odMoney = odMoney;
    }

    public String getOdGd() {
        return odGd;
    }

    public void setOdGd(String odGd) {
        this.odGd = odGd;
    }

    public String getPdName() {
        return pdName;
    }

    public void setPdName(String pdName) {
        this.pdName = pdName;
    }
}
