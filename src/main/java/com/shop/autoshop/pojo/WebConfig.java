package com.shop.autoshop.pojo;

public class WebConfig {
    private String webName;
    private String webDes;
    private String webKeywords;
    private String webUrl;
    private String webPayToken;
    private String webPayId;

    public String getWebName() {
        return webName;
    }

    public void setWebName(String webName) {
        this.webName = webName;
    }

    public String getWebDes() {
        return webDes;
    }

    public void setWebDes(String webDes) {
        this.webDes = webDes;
    }

    public String getWebKeywords() {
        return webKeywords;
    }

    public void setWebKeywords(String webKeywords) {
        this.webKeywords = webKeywords;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getWebPayToken() {
        return webPayToken;
    }

    public void setWebPayToken(String webPayToken) {
        this.webPayToken = webPayToken;
    }

    public String getWebPayId() {
        return webPayId;
    }

    public void setWebPayId(String webPayId) {
        this.webPayId = webPayId;
    }
}
