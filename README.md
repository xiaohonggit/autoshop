# 自动发货系统 基于SSM

#### 介绍
自动发货系统 基于SSM 使用第三方码支付

#### 软件架构
前端使用了layui  后端主要使用SSM  Thymeleaf 模板引擎 数据库Mysql 5.6  腾讯防水墙 


#### 安装教程

1.  克隆代码
2.  导入数据库
3.  申请腾讯防水墙( https://007.qq.com/ )
4.  在 \src\main\java\com\shop\autoshop\controller\AjaxController.java 里修改Aid和AppKey
5.  登录后台 账号admin 密码admin
6.  注册码支付( https://codepay.fateqq.com/ )
7.  网站信息中修改码支付 payId payToken

#### 使用说明

1.  本系统需要安装Maven
2.  本系统会有定时的过期订单处理（打印控制台）定时时间 AutoshopApplication.java 中修改
3.  本系统可用于卡密/链接销售
4.  部分前端模板是基于互联网 请注意版权归属 
5.  部分简单功能未完成 有能力朋友可以自己添加一下 顺便练练手
6.  建议更换前端模板

#### 系统图片
![首页](https://images.gitee.com/uploads/images/2020/0309/000020_8c1903fd_5171550.png "商品自动发货系统.png")
![商品详情](https://images.gitee.com/uploads/images/2020/0309/000053_6aa43823_5171550.png "商品.png")
![查询页面](https://images.gitee.com/uploads/images/2020/0309/000118_e23624ed_5171550.png "Letter - Simple Sign Up Form.png")
![后台界面](https://images.gitee.com/uploads/images/2020/0309/000337_186ceb30_5171550.png "后台管理.png")


#### 相关流程
![数据流图](https://images.gitee.com/uploads/images/2020/0309/000450_bafb99ae_5171550.png "数据流图.png")
![PDM](https://images.gitee.com/uploads/images/2020/0309/000511_07fae96d_5171550.png "pdm.png")


