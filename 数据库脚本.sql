﻿# Host: 127.0.0.1:3307  (Version 5.6.35-log)
# Date: 2019-11-15 14:15:48
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "s_user"
# 管理员信息表
#

CREATE TABLE `s_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_power` int(11) NOT NULL,
  `user_state` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "s_user"
#

INSERT INTO `s_user` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3',1,1);

#
# Structure for table "s_class"
# 商品分类表
#

CREATE TABLE `s_class` (
  `cl_id` int(11) NOT NULL AUTO_INCREMENT,
  `cl_name` varchar(20) NOT NULL,
  `cl_des` varchar(255) DEFAULT NULL,
  `cl_addtime` datetime NOT NULL,
  `cl_state` int(11) NOT NULL,
  `cl_user` int(11) NOT NULL,
  PRIMARY KEY (`cl_id`),
  KEY `FK_Reference_5` (`cl_user`),
  CONSTRAINT `FK_Reference_5` FOREIGN KEY (`cl_user`) REFERENCES `s_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "s_class"
#

INSERT INTO `s_class` VALUES (1,'产品分类1','这是产品分类2','2019-09-06 19:07:00',1,1),(2,'产品分类2','这是产品分类22','2019-09-06 21:21:24',1,1);

#
# Structure for table "s_product"
# 商品信息表
#

CREATE TABLE `s_product` (
  `pd_id` int(11) NOT NULL AUTO_INCREMENT,
  `pd_name` varchar(100) NOT NULL,
  `pd_type` int(11) NOT NULL DEFAULT '0' COMMENT '0 卡密 1固定',
  `pd_gd` text,
  `pd_gd_num` int(11) unsigned DEFAULT NULL,
  `pd_price` double NOT NULL DEFAULT '0',
  `pd_price2` double DEFAULT NULL,
  `pd_pic` text NOT NULL,
  `pd_des` longtext,
  `pd_addtime` datetime NOT NULL,
  `pd_class` int(11) NOT NULL,
  `pd_state` int(11) NOT NULL DEFAULT '0' COMMENT '0 下架 1 上架',
  `pd_user` int(11) NOT NULL,
  PRIMARY KEY (`pd_id`),
  KEY `FK_Reference_1` (`pd_user`),
  KEY `FK_Reference_4` (`pd_class`),
  CONSTRAINT `FK_Reference_1` FOREIGN KEY (`pd_user`) REFERENCES `s_user` (`user_id`),
  CONSTRAINT `FK_Reference_4` FOREIGN KEY (`pd_class`) REFERENCES `s_class` (`cl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

#
# Data for table "s_product"
#

INSERT INTO `s_product` VALUES (1,'测试商品1',0,NULL,NULL,0.01,100,'/uploads/15737982258390.jpg','<p style=\"text-align: center;\">欢迎使用 <b>商品自动销售系统</b></p><p style=\"text-align: center;\"><b style=\"color: rgb(194, 79, 74);\">这是测试商品1的简介</b></p><p><br></p>','2019-09-10 11:33:07',1,1,1),(2,'测试商品2',0,NULL,NULL,0.01,100,'/uploads/15737982258390.jpg','<p>欢迎使用 <b>商品自动销售系统</b></p><p><br></p>','2019-09-10 13:58:52',2,1,1),(3,'产品3',1,'http://baidu.com',1,0.01,100,'/uploads/15737982258390.jpg','<p>欢迎使用 <b>商品自动销售系统</b></p><p><br></p>','2019-09-10 21:06:11',1,1,1);

#
# Structure for table "s_order"
# 订单信息表
#

CREATE TABLE `s_order` (
  `od_id` int(11) NOT NULL AUTO_INCREMENT,
  `od_pd_id` int(11) NOT NULL,
  `od_addtime` datetime NOT NULL,
  `od_pay_id` text NOT NULL,
  `od_order_id` text NOT NULL,
  `od_ts_num` text COMMENT '交易流水号',
  `od_state` int(11) NOT NULL,
  `od_buy_num` int(11) NOT NULL,
  `od_money` double DEFAULT NULL,
  `od_good_id` text,
  `od_contact` varchar(200) NOT NULL,
  `od_password` varchar(30) NOT NULL,
  `od_pay_method` int(11) NOT NULL DEFAULT '0' COMMENT '1 支付宝 2 QQ',
  `od_gd` text,
  PRIMARY KEY (`od_id`),
  KEY `FK_Reference_2` (`od_pd_id`),
  CONSTRAINT `FK_Reference_2` FOREIGN KEY (`od_pd_id`) REFERENCES `s_product` (`pd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

#
# Data for table "s_order"
#

INSERT INTO `s_order` VALUES (9,3,'2019-09-23 13:51:41','15692179017100','177117442535','2019092322001475191054561528',1,1,0.01,'','1073437487','1073437487',1,'http://baidu.com'),(10,1,'2019-09-23 13:59:40','15692183796990','177116111374','2019092322001475191054607005',1,1,0.01,'3','1073437487','1073437487',1,''),(11,1,'2019-09-23 14:15:06','15692193055830','177118181315','2019092322001475191054551719',1,1,0.01,'4','430141','430141',1,''),(18,3,'2019-09-24 09:31:33','15692886937390','177412074311','2019092422001475190549033079',1,1,0.01,'','430141','430141',1,'http://baidu.com'),(23,2,'2019-11-15 11:24:02','15737882422700','173518447298','2019111522001475195729707841',1,1,0.02,'6','430141','430141',1,''),(25,2,'2019-11-15 11:32:39','15737887593010','173510234331','2019111522001475195729543440',1,1,0.02,'7','1073437487','1073437487',1,'');

#
# Structure for table "s_stock"
# 库存信息表
#

CREATE TABLE `s_stock` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_pd_id` int(11) NOT NULL,
  `st_kh` text,
  `st_mm` text,
  `st_addtime` datetime NOT NULL,
  `st_state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`st_id`),
  KEY `FK_Reference_6` (`st_pd_id`),
  CONSTRAINT `FK_Reference_6` FOREIGN KEY (`st_pd_id`) REFERENCES `s_product` (`pd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

#
# Data for table "s_stock"
#

INSERT INTO `s_stock` VALUES (4,1,'smdksmdksa','1232132321','2019-09-12 14:30:23',1),(5,2,'bhcbdshcbdh','jdcbjdscjdsc','2019-11-14 15:02:27',0),(6,2,'cdscdscdsc','dscdcdscdc','2019-11-14 15:02:47',1),(7,2,'jsubhcbdhc','jbcdjkssdds','2019-11-14 15:02:47',1),(8,1,'dcdbchdsbcdshk','cbdjskcbdjsbc','2019-11-14 15:03:03',0),(9,1,'dcvdhcdcbbcbc','ncdncjdncjss','2019-11-14 15:03:03',0);

#
# Structure for table "s_web"
# 网站配置信息表
#

CREATE TABLE `s_web` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `web_name` varchar(200) DEFAULT NULL,
  `web_des` varchar(255) DEFAULT NULL,
  `web_keywords` varchar(255) DEFAULT NULL,
  `web_url` text,
  `web_pay_token` varchar(255) DEFAULT NULL,
  `web_pay_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

#
# Data for table "s_web"
#

INSERT INTO `s_web` VALUES (1,'商品自动发货系统','商品自动发货系统简介','商品,自动,发货,系统,autoshop','http://localhost:8080/','qingxiuigai','qingxiugai');
